import Vue from 'vue'
import Router from 'vue-router'
import overviewView from 'views/overview'

Vue.use(Router);

const router = new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      redirect: '/overview'
    },
    {
      path: '/overview',
      component: overviewView
    },
    {
      path: '/*',
      redirect: '/'
    }
  ]
});

export default router;

